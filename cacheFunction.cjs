function cacheFunction(cb) {
    const cache = [];
   // console.log(typeof(cache));
    function invoke(arg) {
        // console.log(arg)
        if (cache.find(element => {
            return element === arg;
        })) {
           // console.log(cache);
            return cache;
        }
        else {
            cache.push(arg);
            return cb();
        }
    }
    console.log("callbacking");
    return invoke;
}
function cb() {
    return 1;
}
module.exports.cacheFunction = cacheFunction;
module.exports.cb = cb;