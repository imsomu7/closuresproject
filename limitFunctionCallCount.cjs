function limitFunctionCallCount(cb, n) {
    function infinite() {
        if (n > 0) {

            return cb();
            n--;
        }
        else {
            return null;
        }
    }

    return infinite;
}
function cb() {
    return 1;
}

module.exports.limitFunctionCallCount = limitFunctionCallCount;
// module.exports.infinite = infinite;
module.exports.cb = cb;