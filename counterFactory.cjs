function counterfactory (){
    let count = 0;
    const calc = {
        increment(){
            count ++ ;
            return count;
        },
        decrement(){
            count -- ;
            return count;
        }
    }
    return calc;
}
module.exports = counterfactory;